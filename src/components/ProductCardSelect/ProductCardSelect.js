import React, { useState } from "react";
import { Card } from "react-bootstrap";
import "./ProductCardSelect.scss";

const ProductCardSelect = props => {
  const { product, selectProduct, deselectProduct } = props;
  const [selected, setSelected] = useState(false);

  const selectProductCard = () => {
    if (selected === false) {
      selectProduct(product);
      const element = document.getElementById(`product-${product._id}`);
      element.classList.add("focus");
      setSelected(true);
    } else {
      deselectProduct(product);
      const element = document.getElementById(`product-${product._id}`);
      element.classList.remove("focus");
      setSelected(false);
    }
  };
  return (
    <div className="productcard-selected">
      <div
        className="card-selected"
        id={`product-${product._id}`}
        data-test-id={
          product._id === "620967386a6d1eddfdcdc239"
            ? "miBoxIdEdit-popup-seleccionar-producto"
            : ""
        }
      >
        <Card key={product._id} onClick={selectProductCard}>
          <Card.Img
            className="product__image__selected"
            variant="top"
            src={product.url}
          />
          <Card.Body className="card-body-selected">
            <Card.Title>{product.name}</Card.Title>
            <Card.Text>S/. {product.price}</Card.Text>
          </Card.Body>
        </Card>
      </div>
    </div>
  );
};

export default ProductCardSelect;
