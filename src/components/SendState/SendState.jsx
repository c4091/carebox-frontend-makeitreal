import { ReactComponent as EnviadoIcon } from "../../assets/svg/enviadoIcon.svg";
import { ReactComponent as PorenviarIcon } from "../../assets/svg/porenviarIcon.svg";
import "./SendState.scss";

const SendState = ({ deliveredThisMonth }) => {
  return (
    <div className="SendState">
      <span className={deliveredThisMonth === false ? "active" : ""}>
        <PorenviarIcon /> <b>Por enviar</b>
      </span>
      <span className={deliveredThisMonth === true ? "active" : ""}>
        <EnviadoIcon /> <b>Enviado</b>
      </span>
    </div>
  );
};

export default SendState;
