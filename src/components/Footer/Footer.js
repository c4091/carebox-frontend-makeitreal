import React from "react";
import { ReactComponent as Logo } from "../../assets/svg/logoCB.svg";
import "./Footer.scss";

const Footer = () => {
  return (
    <>
      <div className="footer">
        <div className="footer__section">
          <Logo className="footer__logo" width={90} fill="#9CAFEC" />
          <span>Carebox ©2022</span>
        </div>
      </div>
    </>
  );
};
export default Footer;
