import { Grid } from "@mui/material";
import Dialog from "@mui/material/Dialog";
import TextField from "@mui/material/TextField";
import React, { useState } from "react";
import { Card } from "react-bootstrap";
import PayButton from "../PayButton/PayButton";
import { ReactComponent as CardIcon } from "../../assets/svg/creditCard.svg";
import "./SubscriptionDialog.scss";

const SubscriptionDialogCard = props => {
  const { box, handleClose } = props;
  const [deliveryDate, setDeliveryDate] = useState(
    new Date().toLocaleDateString("en-CA")
  );

  const handleInputChangeDeliveryDate = event => {
    setDeliveryDate(event.target.value);
  };

  return (
    <div className="subscription-dialog">
      <Card>
        <Card.Body>
          <CardIcon className="icon" />
          <Card.Title className="mb-4">Realiza tu Subscripción</Card.Title>
          <Grid item xs={6}>
            <TextField
              label="Fecha de envío"
              type="date"
              value={deliveryDate}
              className="mb-3"
              onChange={handleInputChangeDeliveryDate}
            />
          </Grid>
          <div className="btns">
            <PayButton box={box} deliveryDate={deliveryDate} />
            <button
              type="button"
              className="btn btn-dark m-1"
              onClick={handleClose}
            >
              Regresar
            </button>
          </div>
        </Card.Body>
      </Card>
    </div>
  );
};

const SubscriptionDialog = props => {
  const { open, handleClose, box } = props;
  return (
    <Dialog open={open} onClose={handleClose}>
      <SubscriptionDialogCard
        box={box}
        handleClose={handleClose}
      ></SubscriptionDialogCard>
    </Dialog>
  );
};

export default SubscriptionDialog;
