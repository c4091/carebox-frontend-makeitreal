import { css } from "@emotion/react";
import { purple } from "@mui/material/colors";
import { createTheme, ThemeProvider } from "@mui/material/styles";
import TextField from "@mui/material/TextField";
import React, { useState } from "react";
import { Link } from "react-router-dom";
import RingLoader from "react-spinners/RingLoader";
import "./Formbasic.scss";

const override = css`
  display: block;
  margin: 0 auto;
  border-color: "#fff";
`;

const theme = createTheme({
  palette: {
    primary: {
      // Purple and green play nicely together.
      //   main: purple[500],
      main: purple[500],
    },
  },
});
const Formbasic = ({ handleLogin, loading, error }) => {
  // const [email, setEmail] = useState();
  // const [password, setPassword] = useState();

  const [form, setForm] = useState({ email: "", password: "" });
  const handleForm = (e) => {
    setForm({
      ...form,
      [e.target.name]: e.target.value,
    });
  };
  return (
    <form
      className="formbasicLogin"
      onSubmit={(e) => {
        e.preventDefault();
        handleLogin(form);
      }}
    >
      <div className="formbasicLogin__title mb-1">Iniciar Sesión</div>
      <div className="formbasicLogin__subtitle mb-5">
        Ingresa tus credenciales
      </div>

      <ThemeProvider theme={theme}>
        <TextField
          data-test-id="email-input"
          id="outlined-basic"
          label="Email"
          variant="outlined"
          name="email"
          fullWidth
          type="email"
          className="mb-5"
          value={form.email}
          placeholder="Ingresa tu correo electrónico"
          focused
          disabled={loading}
          onChange={handleForm}
        />
        <TextField
          data-test-id="passwd-input"
          id="outlined-basic"
          label="Password"
          variant="outlined"
          name="password"
          fullWidth
          type="password"
          className="mb-5"
          value={form.password}
          placeholder="Ingresa tu contraseña"
          focused
          disabled={loading}
          onChange={handleForm}
        />
      </ThemeProvider>
      {/* {error ? (
        <>
          <div className="formbasicLogin__space">
            <Stack sx={{ width: "100%", border: "1px solid red" }} spacing={2}>
              <Alert severity="error">{error}</Alert>
            </Stack>
          </div>
        </>
      ) : (
        <div></div>
      )} */}
      {form.password === "" || form.email === "" ? (
        <button type="button" className="button" disabled>
          Ingresar
        </button>
      ) : (
        <button
          data-test-id="login-button"
          // type="button"
          className="button"
          // onClick={() => {
          //   handleLogin(form);
          // }}
          disabled={loading}
        >
          {loading ? (
            <RingLoader color={"#fff"} css={override} size={40} />
          ) : (
            "Ingresar"
          )}
        </button>
      )}
      <div className="formbasicLogin__texto">
        ¿No tienes una cuenta?
        <Link to="/registro">
          <span className="formbasicLogin__texto--link">Regístrate</span>
        </Link>
      </div>
      <div className="formbasicLogin__texto">
        ¿Olvidaste tu contraseña?
        <Link to="/contrasena">
          <span className="formbasicLogin__texto--link">Recupérala</span>
        </Link>
      </div>
    </form>
  );
};

export default Formbasic;
