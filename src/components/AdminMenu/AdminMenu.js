import React, { useContext } from "react";
import "./AdminMenu.scss";
import { Link } from "react-router-dom";
import { ReactComponent as Logo } from "../../assets/svg/logoCB.svg";
import Tarjeta from "../../assets/tarjeta.png";
import { useNavigate } from "react-router-dom";
import { types } from "../../types/types";
import { AuthContext } from "../../auth/authContext";

const AdminMenu = ({ type }) => {
  const navigate = useNavigate();
  const { dispatch } = useContext(AuthContext);

  const handleLogout = () => {
    dispatch({ type: types.logoutAdmin });

    navigate("/admin", {
      replace: true,
    });
  };
  const Options = () => {
    if (type === "today") {
      return (
        <>
          <div className="btn">
            <span>
              <img src={Tarjeta} className="image" alt="tarjeta" />
              <label>Hoy</label>
            </span>
          </div>
          <div className="btn">
            <Link to="/admin/suscripciones">
              <img src={Tarjeta} className="image" alt="bolsa" />
              <label>Todo</label>
            </Link>
          </div>
        </>
      );
    } else {
      return (
        <>
          <div className="btn">
            <Link to="/admin/suscripcionesDiarias">
              <img src={Tarjeta} className="image" alt="tarjeta" />
              <label className="">Hoy</label>
            </Link>
          </div>
          <div className="btn">
            <span>
              <img src={Tarjeta} className="image" alt="bolsa" />
              <label>Todo</label>
            </span>
          </div>
        </>
      );
    }
  };

  return (
    <div className=" col col-4  col-md-3 col-lg-2 adminSup__section__left">
      <div className="adminSup__graphicloginBox2">
        <div className="adminSup__graphicloginBox2__info">
          <Logo fill="white" />
          <p className="adminSup__graphicloginBox2__info__text">Admin</p>
        </div>
      </div>
      <label className="adminSup__section__left__sublabel">SECCIONES</label>
      <Options />
      <div className="adminSup__graphicloginBox2__button">
        <button type="button" className="btn btn-dark" onClick={handleLogout}>
          Cerrar Session
        </button>
      </div>
    </div>
  );
};

export default AdminMenu;
