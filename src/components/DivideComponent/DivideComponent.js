import { Fade } from "react-reveal";
import "./DivideComponent.scss";

const DivideComponent = props => {
  return (
    <main>
      <div className="divide-component">
        <Fade>
          <div className="divide-component__left">
            <h1>{props.left.title}</h1>
            <p>{props.left.subtitle}</p>
            <div className="divide-component__left__form">
              {props.left.component}
            </div>
          </div>
        </Fade>

        <div className="divide-component__right">
          <div>{props.right.title}</div>
          <div>{props.right.subtitle}</div>
          <Fade>
            <div className="divide-component__right__scroll">
              {props.right.component}
            </div>
          </Fade>
        </div>
      </div>
    </main>
  );
};

export default DivideComponent;
