import React from "react";
import { Card } from "react-bootstrap";
import "./ProductCard.scss";

const ProductCard = props => {
  const { product, canDelete, deleteProduct } = props;

  const deleteProductFromCard = () => {
    if (canDelete === true) {
      deleteProduct(product);
    }
  };

  const Buttons = () => {
    if (canDelete === true) {
      return (
        <button
          className="btn btn-danger"
          onClick={deleteProductFromCard}
          data-test-id={
            product._id === "620967386a6d1eddfdcdc239"
              ? "miBoxIdEdit-eliminar-button"
              : ""
          }
        >
          <i className="fa fa-trash-o" aria-hidden="true"></i> Eliminar
        </button>
      );
    } else return <></>;
  };

  return (
    <div className="productcard">
      <Card key={product._id}>
        <Card.Img className="product__image" variant="top" src={product.url} />
        <Card.Body className="card-body">
          <Card.Title>{product.name}</Card.Title>
          <Card.Text>S/. {product.price}</Card.Text>
          <Buttons></Buttons>
        </Card.Body>
      </Card>
    </div>
  );
};

export default ProductCard;
