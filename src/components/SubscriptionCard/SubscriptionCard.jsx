import React from "react";
import { Button } from "react-bootstrap";
import { Link } from "react-router-dom";
import { ReactComponent as BoxIcon } from "../../assets/svg/boxicon.svg";
import { cancelSubscription } from "../../services/SubscriptionService";
import SendState from "../SendState/SendState";
import { dateEs, getNextDate, getRemainTime } from "./dateFunctions";
import "./SubscriptionCard.scss";

const SubscriptionCard = ({
  subscription,
  standard,
  getSubscriptionsFromApi,
}) => {
  const cancelFromApi = () => {
    cancelSubscription(subscription._id).then((data) => {
      if (data.status === 1) {
        getSubscriptionsFromApi();
      }
    });
  };

  const btnDelete = () => (
    <Link to={{ pathname: "" }}>
      <Button
        className="btn btn-care btn-block px-3 py-2"
        onClick={cancelFromApi}
      >
        Cancelar suscripción
      </Button>
    </Link>
  );

  return (
    <div className={"subscriptioncard"} key={subscription._id}>
      <div className="subscriptioncard__box">
        <Link
          to={{
            pathname: `/${standard ? "box" : "mibox"}/details/${
              subscription.box._id
            }`,
          }}
        >
          <div>
            <BoxIcon />
          </div>
          <div>
            <div className="subscriptioncard__name">
              {subscription.box.name}
            </div>
            <div className="subscriptioncard__price">
              S/. {subscription.price}
            </div>
          </div>
        </Link>
      </div>

      <div className="subscriptioncard__date">
        <small>FECHA DE SUSCRIPCIÓN</small>
        <div>{dateEs(subscription.deliveryDate)}</div>
      </div>

      <div className="subscriptioncard__next">
        <small>PRÓXIMA DEBITACIÓN</small>
        <div>{getRemainTime(getNextDate(subscription.deliveryDate))}</div>
      </div>

      <div className="subscriptioncard__send">
        <small>ESTADO DEL PEDIDO</small>
        <SendState deliveredThisMonth={subscription.deliveredThisMonth} />
      </div>

      <div className="subscriptioncard__actions">{btnDelete()}</div>
    </div>
  );
};

export default SubscriptionCard;

//
