// funcion que retorna la fecha en español
export const dateEs = str => {
  const [anho, mes, dia] = str.split("-");
  return `${dia} ${getMesEs[Number(mes - 1)]} ${anho}`;
};

// funcion que retorna la siguiente fecha decha facturación
// si el número de día actual es mayor al día de facturacion aumenta el mes en 1
export const getNextDate = str => {
  const [, , dia] = str.split("-");
  const [currentY, currentM, currentD] = getNow().split("-");
  console.log(currentM);
  return `${
    dia < currentD && currentM === "12" ? Number(currentY) + 1 : currentY
  }-${
    dia < currentD
      ? Number(currentM) + 1 > getMesEs.length
        ? 1
        : Number(currentM) + 1
      : currentM
  }-${dia}`;
};

// funcion que retorna la cantidad de días que falta para la siguiente facturación
// restamos la fecha de facturacion en milisegundo con la fecha actual en milisegundos
// y lo retornamos en días
export const getRemainTime = str => {
  const [anho, mes, dia] = str.split("-");
  const remainTime = new Date(`${anho}/${mes}/${dia}`).getTime() - Date.now();

  return `En ${Math.round(remainTime / 1000 / 60 / 60 / 24)} días`;
  //   return str;
};

// function que retorna la fecha actual en el formato "año-mes-dia"
const getNow = () =>
  `${new Date().getFullYear()}-${
    new Date().getMonth() + 1
  }-${new Date().getDate()}`;

const getMesEs = [
  "enero",
  "febrero",
  "marzo",
  "abril",
  "mayo",
  "junio",
  "julio",
  "agosto",
  "septiembre",
  "octubre",
  "noviembre",
  "diciembre",
];
