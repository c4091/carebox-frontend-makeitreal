import React from 'react'
import { Link } from 'react-router-dom';
import iconsmile from "../../assets/iconsmile.png"
import "./Indications.scss";

const Indications = (props) => {
    const {title,button,link, color, description} = props
    let isValid
    if (color === true) {
        isValid = true
    }
    else
    {
        isValid = false
    }
    return (
        <div className="indications">
            
            <div    className={`indications__title${isValid ? ' casoone__title':' casotwo__title'}`}>
                {title} <img src={iconsmile} className="indications__title__image" alt =""/>
            </div>
            <div className={`indications__description${isValid ? ' casoone__description':' casotwo__description'}`}>
                {description}
            </div>
            <div className="indications__button">
                <Link to={link}>
                    <button type="button" className="button">{button}</button>
                </Link>
            </div>
        </div>
    )
}
export default Indications;