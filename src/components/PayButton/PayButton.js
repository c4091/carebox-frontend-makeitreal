const PayButton = ({ box, deliveryDate }) => {
  const handler = window.ePayco.checkout.configure({
    key: process.env.REACT_APP_EPAYCO_APIKEY,
    test: true,
  });

  let data = {
    name: "Suscripción",
    description: box.name,
    currency: "cop",
    amount: 12000,
    tax_base: "0",
    tax: "0",
    country: "co",
    lang: "en",
    external: "false",
    type_doc_billing: "cc",
    response: `${process.env.REACT_APP_REDIRECT_PAYMENT}/validatePayment/${box._id}/${deliveryDate}`,
    methodsDisable: ["PSE", "SP", "CASH", "DP"],
  };

  const openPayment = () => {
    handler.open(data);
  };

  return (
    <>
      <button type="button" className="btn btn-care m-1" onClick={openPayment}>
        Suscribirme
      </button>
    </>
  );
};

export default PayButton;
