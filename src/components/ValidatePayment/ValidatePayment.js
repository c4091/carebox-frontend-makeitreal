import { useContext, useEffect } from "react";
import { useLocation, useParams } from "react-router-dom";
import { getValidation } from "../../services/PaymentService";
import { createSubscription } from "../../services/SubscriptionService";
import { useNavigate } from "react-router-dom";
import { AuthContext } from "../../auth/authContext";
import { RingLoader } from "react-spinners";
import { css } from "@emotion/react";

const override = css`
  display: block;
  margin: 0 auto;
  border-color: "#fff";
`;

const ValidatePayment = () => {
  let navigate = useNavigate();
  const { idBox, deliveryDate } = useParams();
  const search = useLocation().search;
  const idReference = new URLSearchParams(search).get("ref_payco");
  const { user } = useContext(AuthContext);

  const getValidationFromApi = async () => {
    console.log(idBox);
    getValidation(idReference).then((data) => {
      console.log(data);
      if (data.success === true) {
        generateSubscription();
      } else {
        navigate("../box", { replace: true });
      }
    });
  };

  const generateSubscription = () => {
    const subscription = {
      idClient: user.userCarebox.idClient,
      deliveryDate: deliveryDate,
      idBox: idBox,
    };
    console.log(subscription);

    createSubscription(subscription).then((data) => {
      if (data.status === 1) {
        navigate("../suscripciones", { replace: true });
      } else {
        navigate("../box", { replace: true });
      }
    });
  };

  useEffect(() => {
    getValidationFromApi();
    // eslint-disable-next-line
  }, [idBox, idReference, deliveryDate]);

  return (
    <div style={{ marginTop: 150, marginBottom: 100 }}>
      <div className="adminLogin__form__tittle" style={{ color: "black" }}>
        Validando...
      </div>
      <br></br>
      <RingLoader color={"#6610f2"} css={override} size={100} loading={true} />
    </div>
  );
};

export default ValidatePayment;
