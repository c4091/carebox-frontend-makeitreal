import httpClient from "../utils/httpClient";

export const registerUser = async (register) => {
  const data = await httpClient
    .post("/login/registerClient", register)
    .then((v) => {
      return v.data;
    });
  return data;
};
