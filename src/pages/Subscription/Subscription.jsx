import React, { useEffect, useState } from "react";
import { Fade } from "react-reveal";
import SubscriptionCard from "../../components/SubscriptionCard/SubscriptionCard";
import { getSubscriptions } from "../../services/SubscriptionService";
import "./Subscription.scss";
import { useContext } from "react";
import { AuthContext } from "../../auth/authContext";

const Subscription = () => {
  const [subscriptions, setSubscriptions] = useState([]);
  const { user } = useContext(AuthContext);

  const getSubscriptionsFromApi = async () => {
    getSubscriptions(user.userCarebox.idClient).then((data) => {
      if (data.subscriptions) {
        setSubscriptions(data.subscriptions);
      }
    });
  };

  useEffect(() => {
    getSubscriptionsFromApi();
    // eslint-disable-next-line
  }, []);

  return (
    <main>
      <div className="suscription">
        <div className="suscription__header">
          <div className="container">
            <h1>Suscripciones</h1>
            <div>{subscriptions.length} resultados</div>
          </div>
        </div>
        <div className="suscription__body">
          <Fade>
            <div className="container">
              {subscriptions.map((subscription) => {
                return (
                  <SubscriptionCard
                    subscription={subscription}
                    standard={true}
                    getSubscriptionsFromApi={getSubscriptionsFromApi}
                  ></SubscriptionCard>
                );
              })}
            </div>
          </Fade>
        </div>
      </div>
    </main>
  );
};

export default Subscription;
