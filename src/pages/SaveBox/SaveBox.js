import TextField from "@mui/material/TextField";
import React, { useEffect, useState } from "react";
import { Card } from "react-bootstrap";
import { Link, useNavigate, useParams } from "react-router-dom";
import addIcon from "../../assets/add.png";
import DivideComponent from "../../components/DivideComponent/DivideComponent";
import ProductCard from "../../components/ProductCard/ProductCard";
import ProductDialog from "../../components/ProductDialog/ProductDialog";
import {
  createBox,
  getBoxById,
  updateBox,
  uploadImageBox,
} from "../../services/MyBoxService";
import "./SaveBox.scss";
import { useContext } from "react";
import { AuthContext } from "../../auth/authContext";

const SaveBox = () => {
  const { idBox } = useParams();
  const { user } = useContext(AuthContext);
  const [title, setTitle] = useState("");
  const [file, setFile] = useState(null);

  const [box, setBox] = useState({
    idBox: idBox,
    name: "",
    price: 0,
    products: [],
    image: null,
  });

  const getBoxFromApi = async () => {
    getBoxById(idBox).then(data => {
      if (data.box) {
        setBox(data.box);
      }
    });
  };

  useEffect(() => {
    if (idBox !== "0") {
      getBoxFromApi();
      setTitle("Editar Box");
    } else {
      setTitle("Crear Box");
    } // eslint-disable-next-line
  }, []);

  const calculatePrice = newProducts => {
    let price = 0;
    newProducts.forEach(product => {
      price += product.price;
    });
    return price;
  };

  const addProducts = products => {
    const newProducts = box.products.concat(products);
    let boxAux = {
      ...box,
      products: newProducts,
      price: calculatePrice(newProducts),
    };
    setBox(boxAux);
  };

  const deleteProduct = product => {
    const productsAux = box.products;
    const index = productsAux.findIndex(
      productAux => product._id === productAux._id
    );
    productsAux.splice(index, 1);
    let boxAux = {
      ...box,
      products: productsAux,
      price: calculatePrice(productsAux),
    };
    setBox(boxAux);
  };

  const getIdProducts = () => {
    const idProducts = [];
    box.products.forEach(product => {
      idProducts.push(product._id);
    });
    return idProducts;
  };

  const LeftComponent = () => {
    let navigate = useNavigate();

    const save = () => {
      if (idBox === "0") {
        let boxCreateDTO = {
          idClient: user.userCarebox.idClient,
          idProducts: getIdProducts(),
          name: box.name,
          image: box.image,
        };

        uploadImageBox(file).then(data => {
          if (data.status === 1) {
            boxCreateDTO.image = data.result;
          }
          createBox(boxCreateDTO).then(data => {
            if (data.status === 1) {
              navigate(`../mibox/details/${data.box._id}`, { replace: true });
            }
          });
        });
      } else {
        const boxUpdateDTO = {
          idClient: user.userCarebox.idClient,
          idBox: idBox,
          idProducts: getIdProducts(),
          name: box.name,
          image: box.image,
        };

        uploadImageBox(file).then(data => {
          if (data.status === 1) {
            boxUpdateDTO.image = data.result;
          }
          updateBox(boxUpdateDTO).then(data => {
            if (data.status === 1) {
              navigate(`../mibox/details/${data.box._id}`, { replace: true });
            }
          });
        });
      }
    };

    const handleInputChange = event => {
      let boxAux = { ...box, name: event.target.value };
      setBox(boxAux);
    };

    const handleFilesChange = event => {
      console.log();
      setFile(event.target.files[0]);
    };

    return (
      <>
        <TextField
          data-test-id="miBoxIdEdit-editarName-input"
          autoFocus={true}
          size="small"
          label="Nombre del box"
          variant="outlined"
          name="name"
          fullWidth
          type="text"
          className="mb-3"
          value={box.name}
          onChange={handleInputChange}
        />

        {!file && !!box.image && (
          <div>
            <img src={box.image} alt={box.name} width="100px" />
            <br />
            <br />
          </div>
        )}

        <label htmlFor="icon-button-file" className="btn btn-care">
          <i className="fa fa-picture-o" aria-hidden="true"></i>&nbsp;
          {!!file ? "Cambiar imagen" : "Cargar imagen"}
        </label>

        <input
          data-cy="miBoxIdEdit-file-input"
          color="primary"
          accept="image/*"
          type="file"
          onChange={handleFilesChange}
          id="icon-button-file"
        />

        <div className="file-name">
          {!!file ? (
            <>
              <i className="fa fa-check-circle" aria-hidden="true"></i>&nbsp;
              {file?.name}
            </>
          ) : (
            "No se ha cargado ningun archivo"
          )}
        </div>

        <br></br>
        <button
          type="button"
          className="btn btn-care"
          onClick={save}
          data-test-id="miBoxIdEdit-guardar-button"
        >
          Guardar
        </button>
        <Link
          to={{
            pathname: `/mibox`,
          }}
        >
          <button type="button" className="btn btn-dark m-2">
            Regresar
          </button>
        </Link>
      </>
    );
  };

  const RightComponent = () => {
    const [open, setOpen] = useState(false);

    const handleOpen = () => {
      setOpen(true);
    };

    const handleClose = () => {
      setOpen(false);
    };

    const NewCard = () => {
      return (
        <button
          data-test-id="miBoxIdEdit-agregar-button"
          className="btn btnNew"
          onClick={handleOpen}
        >
          <div className="newcard">
            <Card>
              <Card.Img
                className="newcard__image"
                variant="top"
                src={addIcon}
              />
              <Card.Body className="card-body">
                <Card.Title>Agregar Producto</Card.Title>
              </Card.Body>
            </Card>
          </div>
        </button>
      );
    };

    return (
      <>
        <div className="d-flex align-content-stretch flex-wrap">
          <NewCard></NewCard>
          {box.products.map((product, index) => {
            return (
              <ProductCard
                key={index}
                product={product}
                canDelete={true}
                deleteProduct={deleteProduct}
              ></ProductCard>
            );
          })}
        </div>
        <ProductDialog
          open={open}
          handleClose={handleClose}
          addProducts={addProducts}
        ></ProductDialog>
      </>
    );
  };

  return (
    <>
      <DivideComponent
        left={{
          title: `${title}`,
          subtitle: `S/. ${box.price}`,
          component: <LeftComponent />,
        }}
        right={{
          title: "Productos",
          subtitle: `${box.products.length} resultados`,
          component: <RightComponent />,
        }}
      ></DivideComponent>
    </>
  );
};

export default SaveBox;
