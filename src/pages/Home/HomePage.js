import React, { useContext } from "react";
import { Fade } from "react-reveal";
import boximage1 from "../../assets/boximage1.png";
import boximage2 from "../../assets/boximage2.png";
import boximage3 from "../../assets/boximage3.png";
import { AuthContext } from "../../auth/authContext";
import Indications from "../../components/Indications/Indications";
import "./HomePage.scss";

const HomePage = () => {
  const { user } = useContext(AuthContext);
  let rutaButton1 = "login";
  if (user.userCarebox !== undefined) {
    rutaButton1 = "suscripciones";
  } else {
    rutaButton1 = "login";
  }

  return (
    <>
      <div className=" home">
        <div className=" home__one ">
          <div className=" row home__one__section ">
            <div className="col col-12 col-md-6 home__one__section__left">
              <div className="graphic">
                <Fade left>
                  <img src={boximage1} className="graphic__image1" alt="" />
                </Fade>
              </div>
              <Fade>
                <Indications
                  title={"Suscríbete a un CareBox"}
                  button={"Ir a la Página"}
                  link={rutaButton1}
                  color={true}
                  description={
                    "Suscribirse a un box te da el beneficio de obtenerlo cada mes, por delivery"
                  }
                />
              </Fade>
            </div>
            <Fade up>
              <div className="col col-12 col-md-6 home__one__section__right">
                <div className="graphic">
                  <Fade>
                    <img src={boximage2} className="graphic__image2" alt="" />
                  </Fade>
                </div>
              </div>
            </Fade>
          </div>
        </div>

        <div className="home__two">
          <div className="row home__two__section">
            <div className=" col col-12 col-md-6 home__two__section__left--flexOrder">
              <div className="graphic">
                <Fade up>
                  <img src={boximage3} className="graphic__image3" alt="" />
                </Fade>
              </div>
            </div>
            <Fade>
              <div className="col col-12 col-md-6 home__two__section__right">
                <Indications
                  title={"Arma tu box"}
                  button={"Crear mi Box"}
                  link={"box"}
                  color={false}
                  description={
                    "Suscribirse a un box te da el beneficio de obtenerlo cada mes, por delivery"
                  }
                />
              </div>
            </Fade>
          </div>
        </div>
      </div>
    </>
  );
};

export default HomePage;
