import { css } from "@emotion/react";
import ArrowBackIosIcon from "@mui/icons-material/ArrowBackIos";
import { purple } from "@mui/material/colors";
import { createTheme, ThemeProvider } from "@mui/material/styles";
import TextField from "@mui/material/TextField";
import React, { useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import RingLoader from "react-spinners/RingLoader";
import { toast, ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { restorePassword } from "../../services/LoginService";
import "./RestorePasswordPage.scss";

const override = css`
  display: block;
  margin: 0 auto;
  border-color: "#fff";
`;

const theme = createTheme({
  palette: {
    primary: {
      main: purple[500],
    },
  },
});

const RestorePasswordPage = () => {
  const navigate = useNavigate();
  const [loading, setLoading] = useState(false);
  const [email, setEmail] = useState("");

  const restorePasswordFromApi = () => {
    restorePassword(email).then((data) => {

      if (data.status === 1) {
        setLoading(false);
        toast.info(`${data.message}`, {
          position: toast.POSITION.BOTTOM_LEFT,
          autoClose: 3000,
          pauseOnHover: false,
        });
        navigate("/login", {
          replace: true,
        });
      } else {
        setLoading(false);
        toast.error(`${data.message}`, {
          position: toast.POSITION.BOTTOM_LEFT,
          autoClose: 3000,
          pauseOnHover: false,
        });
      }
    });
  };

  const handleForm = (e) => {
    setEmail(e.target.value);
  };

  return (
    <>
      <div className="register">
        <ToastContainer autoClose={1500} />

        <form
          className="register__form"
          onSubmit={(e) => {
            e.preventDefault();
            restorePasswordFromApi();
          }}
        >
          <Link to="/login">
            <div className="register__form__back mb-1">
              <ArrowBackIosIcon />
              Regresar
            </div>
          </Link>

          <div className="register__form__title mb-1">Recuperar contraseña</div>

          <div className="register__form__subtitle mb-5">
            Por favor inrgese su correo electrónico
          </div>
          <ThemeProvider theme={theme}>
            <div className="row  register__form__datos">
              <div className="col col-12 ">
                <TextField
                  id="outlined-basic"
                  label="Email *"
                  variant="outlined"
                  name="email"
                  value={email}
                  fullWidth
                  type="email"
                  className="mb-4"
                  placeholder="Ingresa tu Correo Electrónico"
                  focused
                  disabled={loading}
                  onChange={handleForm}
                />
              </div>
            </div>
            <div className="row register__form__datos">
              {}
              <div className=" register__form__datos__button">
                {email === "" ? (
                  <button type="button" className="button" disabled>
                    Enviar correo de recuperación
                  </button>
                ) : (
                  <button
                    className="button register__form__textButton"
                    disabled={loading}
                  >
                    {loading ? (
                      <RingLoader color={"#fff"} css={override} size={40} />
                    ) : (
                      "Enviar correo de recuperación"
                    )}
                  </button>
                )}
              </div>
            </div>
          </ThemeProvider>
        </form>
      </div>
    </>
  );
};

export default RestorePasswordPage;
