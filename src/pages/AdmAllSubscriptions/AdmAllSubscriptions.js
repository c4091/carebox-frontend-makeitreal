import Paper from "@mui/material/Paper";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import React, { useEffect, useState } from "react";
import AdminMenu from "../../components/AdminMenu/AdminMenu";
import SubscriptionFilter from "../../components/SubscriptionFilter/SubscriptionFilter";
import {
  getAllSubscriptions,
  getAllSubscriptionsByEmail,
} from "../../services/SubscriptionService";
import "./AdmAllSubscriptions.scss";

const AdmAllSubscriptions = () => {
  const [subscriptions, setSubscriptions] = useState([]);

  const getAllSubscriptionsFromApi = async () => {
    getAllSubscriptions().then(data => {
      if (data.subscriptions) {
        setSubscriptions(data.subscriptions);
      }
    });
  };

  const handleSearch = email => {
    getAllSubscriptionsByEmail(email).then(data => {
      if (data.status === 1) {
        setSubscriptions(data.subscriptions);
      } else if (data.status === 0) {
        setSubscriptions([]);
      }
    });
  };

  useEffect(() => {
    getAllSubscriptionsFromApi();
  }, []);

  return (
    <>
      <div className="adminSup">
        <div className="row adminSup__one">
          <div className=" adminSup__section">
            <AdminMenu type="all" />
            <div className="col col-8 col-sm-6 col-md-9 col-lg-9 adminSup__section__right">
              <div className="adminSup__section__right__tittle">
                <label className="adminSup__section__right__tittle__label">
                  Suscripciones
                </label>
              </div>
              <SubscriptionFilter handleSearch={handleSearch} />
              <div className="row adminSup__section__right__table">
                <TableContainer component={Paper}>
                  <Table sx={{ minWidth: 10 }} aria-label="simple table">
                    <TableHead>
                      <TableRow>
                        <TableCell align="left">Código</TableCell>
                        <TableCell align="left">Nombre</TableCell>
                        <TableCell align="left">Cliente</TableCell>
                        <TableCell align="left">Próxima Entrega</TableCell>
                        <TableCell align="left">Este mes</TableCell>
                      </TableRow>
                    </TableHead>
                    <TableBody>
                      {subscriptions.map(row => (
                        <TableRow
                          key={row._id}
                          sx={{
                            "&:last-child td, &:last-child th": { border: 0 },
                          }}
                        >
                          <TableCell align="left">{row._id}</TableCell>
                          <TableCell align="left">{row.box.name}</TableCell>
                          <TableCell align="left">
                            {row.client.names + " " + row.client.lastNames}
                          </TableCell>
                          <TableCell align="left">{row.deliveryDate}</TableCell>
                          <TableCell align="left">
                            {row.deliveredThisMonth === true
                              ? "Enviado"
                              : "Por enviar"}
                          </TableCell>
                        </TableRow>
                      ))}
                    </TableBody>
                  </Table>
                </TableContainer>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};
export default AdmAllSubscriptions;
