import { purple } from "@mui/material/colors";
import { createTheme, ThemeProvider } from "@mui/material/styles";
import TextField from "@mui/material/TextField";
import React, { useContext, useState } from "react";
import { useNavigate } from "react-router-dom";
import { toast, ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { AuthContext } from "../../auth/authContext";
import { updateUser } from "../../services/UpdateService";
import { types } from "../../types/types";
import "./PerfilPage.scss";

const theme = createTheme({
  palette: {
    primary: {
      main: purple[500],
    },
  },
});

const PerfilPage = () => {
  const navigate = useNavigate();

  const [editStatus, setEditStatus] = useState(true);
  const [editText, setEditText] = useState("Editar");
  const { user } = useContext(AuthContext);

  const [loading, setLoading] = useState(false);
  // eslint-disable-next-line
  const [error, setError] = useState(false);
  const { dispatch } = useContext(AuthContext);
  const [form, setForm] = useState({});

  if (user.userCarebox === undefined) {
    dispatch({ type: types.logout });

    navigate("/login", {
      replace: true,
    });
  }

  const getUserFromApi = form => {
    form.idClient = user.userCarebox.idClient;
    updateUser(form).then(data => {
      const dataClient = data.client;

      if (data.status === 1) {
        setLoading(false);
        const userCarebox = {
          idClient: dataClient._id,
          names: dataClient.names,
          lastNames: dataClient.lastNames,
          phone: dataClient.phone,
          address: dataClient.address,
          userLogin: {
            email: dataClient.userLogin.email,
          },
        };
        const action = {
          type: types.login,
          payload: { userCarebox },
        };
        toast.success(`${data.message}`, {
          position: toast.POSITION.BOTTOM_LEFT,
          pauseOnHover: false,
        });
        dispatch(action);
      } else {
        setLoading(false);
        toast.error(`${data.message}`, {
          position: toast.POSITION.BOTTOM_LEFT,
          pauseOnHover: false,
        });
        setError(data.message);
      }
    });
  };

  const handleUpdate = async form => {
    setEditStatus(!editStatus);
    if (editStatus === true) {
      setEditText("Guardar");

      setForm({
        names: user.userCarebox.names,
        phone: user.userCarebox.phone,
        lastNames: user.userCarebox.lastNames,
        address: user.userCarebox.address,
        idClient: user.userCarebox._id,
      });
    } else {
      setEditText("Editar");
      setLoading(true);
      getUserFromApi(form);
      console.log(form);
    }
  };

  const handleForm = e => {
    setForm({
      ...form,
      [e.target.name]: e.target.value,
    });
    console.log(form);
  };

  return (
    <>
      <div className="perfil">
        <ToastContainer autoClose={2000} />

        <form
          data-test-id="perfil-form-elements"
          className="perfil__form"
          onSubmit={e => {
            e.preventDefault();
            handleUpdate(form);
          }}
        >
          <ThemeProvider theme={theme}>
            <div className="perfil__form__title mb-1">Perfil</div>

            <div className="perfil__form__subtitle mb-5">
              Usted está visualizando sus datos personales.
            </div>

            <div className="row perfil__form__datos">
              <div className=" col col-12 col-md-6 perfil__form__datos__left">
                <TextField
                  data-test-id="perfil-name-input"
                  id="outlined-required"
                  label="Nombres"
                  variant="outlined"
                  name="names"
                  fullWidth
                  type="text"
                  placeholder="Ingresa tu Nombre"
                  className="mb-4"
                  defaultValue={
                    user.userCarebox === undefined ? "" : user.userCarebox.names
                  }
                  value={form.names}
                  onChange={handleForm}
                  focused
                  disabled={editStatus || loading}
                />
                <TextField
                  id="outlined-required"
                  label="Teléfono"
                  variant="outlined"
                  name="phone"
                  fullWidth
                  type="number"
                  placeholder="Ingresa tu Teléfono"
                  className="mb-4"
                  defaultValue={
                    user.userCarebox === undefined ? "" : user.userCarebox.phone
                  }
                  value={form.phone}
                  onChange={handleForm}
                  focused
                  disabled={editStatus || loading}
                />
              </div>
              <div className="col col-12 col-md-6 perfil__form__datos__right">
                <TextField
                  data-test-id="perfil-lastName-input"
                  id="outlined-required"
                  label="Apellidos"
                  variant="outlined"
                  name="lastNames"
                  fullWidth
                  type="text"
                  placeholder="Ingresa tus Apellidos"
                  className="mb-4"
                  defaultValue={
                    user.userCarebox === undefined
                      ? ""
                      : user.userCarebox.lastNames
                  }
                  value={form.lastNames}
                  onChange={handleForm}
                  focused
                  disabled={editStatus || loading}
                />
                <TextField
                  id="outlined-required"
                  label="Dirección"
                  variant="outlined"
                  name="address"
                  fullWidth
                  type="text"
                  placeholder="Ingresa tus Dirección"
                  className="mb-4"
                  defaultValue={
                    user.userCarebox === undefined
                      ? ""
                      : user.userCarebox.address
                  }
                  value={form.address}
                  onChange={handleForm}
                  focused
                  disabled={editStatus || loading}
                />
              </div>
            </div>
            <div className="row  perfil__form__datos">
              <div className="col col-12 ">
                <TextField
                  id="outlined-required"
                  label="Email"
                  variant="outlined"
                  name="email"
                  fullWidth
                  type="email"
                  placeholder="Ingresa tu Correo Electrónico"
                  className="mb-5"
                  defaultValue={
                    user.userCarebox === undefined
                      ? ""
                      : user.userCarebox.userLogin.email
                  }
                  value={form.email}
                  onChange={handleForm}
                  focused
                  disabled
                />
              </div>
            </div>
            <div className="row perfil__form__datos">
              <div className=" perfil__form__button">
                <button
                  data-test-id="perfil-button-editar/guardar"
                  className="button perfil__form__button__textButton "
                >
                  {editText}
                </button>
              </div>
              {/* <div className="col col-12 col-md-6 perfil__form__datos__left">
                <TextField
                  id="outlined-basic"
                  label="Password"
                  variant="outlined"
                  name="password"
                  fullWidth
                  type="password"
                  placeholder="Ingresa tu Contraseña"
                  defaultValue="Contraseña"
                  className="mb-4"
                  value={form.password}
                  onChange={handleForm}
                  focused
                  disabled={editStatus || loading}
                />
              </div>
              <div className="col col-12 col-md-6 perfil__form__datos__right">
                <TextField
                  id="outlined-basic"
                  label="Confirmar Contraseña"
                  variant="outlined"
                  name="password2"
                  fullWidth
                  type="password"
                  placeholder="Confirma tu Contraseña"
                  defaultValue="Contraseña"
                  className="mb-5"
                  value={form.password2}
                  onChange={handleForm}
                  focused
                  disabled={editStatus || loading}
                />
              </div> */}
            </div>
          </ThemeProvider>
        </form>
      </div>
    </>
  );
};

export default PerfilPage;
