import { css } from "@emotion/react";
import ArrowBackIosIcon from "@mui/icons-material/ArrowBackIos";
import { purple } from "@mui/material/colors";
import { createTheme, ThemeProvider } from "@mui/material/styles";
import TextField from "@mui/material/TextField";
import React, { useContext, useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import RingLoader from "react-spinners/RingLoader";
import { toast, ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { AuthContext } from "../../auth/authContext";
import { registerUser } from "../../services/RegisterService";
import { types } from "../../types/types";
import { setToken } from "../../services/TokenService";
import "./RegisterPage.scss";

const override = css`
  display: block;
  margin: 0 auto;
  border-color: "#fff";
`;

const theme = createTheme({
  palette: {
    primary: {
      main: purple[500],
    },
  },
});

const RegisterPage = () => {
  const navigate = useNavigate();
  const [loading, setLoading] = useState(false);
  // eslint-disable-next-line
  const [error, setError] = useState(false);
  // eslint-disable-next-line
  const [error2, setError2] = useState(false);

  const { dispatch } = useContext(AuthContext);

  const [form, setForm] = useState({
    names: "",
    lastNames: "",
    email: "",
    password: "",
    password2: "",
  });

  const registerUserFromApi = form => {
    registerUser(form).then(data => {
      const dataClient = data.client;

      if (data.status === 1) {
        setToken("Bearer " + data.token);
        setLoading(false);
        const userCarebox = {
          idClient: dataClient._id,
          names: dataClient.names,
          lastNames: dataClient.lastNames,
          phone: dataClient.phone,
          address: dataClient.address,
          userLogin: {
            email: dataClient.userLogin.email,
          },
        };
        const action = {
          type: types.login,
          payload: { userCarebox },
        };

        dispatch(action);
        navigate("/perfil", {
          replace: true,
        });
      } else {
        setLoading(false);
        setError(data.message);
        toast.error(`${data.message}`, {
          position: toast.POSITION.BOTTOM_LEFT,
          autoClose: 3000,
          pauseOnHover: false,
        });
      }
    });
  };

  const handleRegister = async form => {
    if (form.password === form.password2) {
      setLoading(true);
      registerUserFromApi(form);
      setError2(false);
    } else {
      setError2("Contraseñas no coinciden");
      setError(false);
      toast.error("Contraseñas no coinciden", {
        position: toast.POSITION.BOTTOM_LEFT,
        autoClose: 3000,
        pauseOnHover: false,
      });
    }
  };

  const handleForm = e => {
    setForm({
      ...form,
      [e.target.name]: e.target.value,
    });
  };

  return (
    <>
      <div className="register">
        <ToastContainer autoClose={1500} />

        <form
          className="register__form"
          onSubmit={e => {
            e.preventDefault();
            handleRegister(form);
          }}
        >
          <Link to="/login">
            <div className="register__form__back mb-1">
              <ArrowBackIosIcon />
              Regresar
            </div>
          </Link>

          <div className="register__form__title mb-1">Registro</div>

          <div className="register__form__subtitle mb-5">
            Por favor complete todos los campos para registrarse
          </div>
          <ThemeProvider theme={theme}>
            <div className="row register__form__datos">
              <div className=" col col-12 col-md-6 register__form__datos__left">
                <TextField
                  id="outlined-basic"
                  label="Names *"
                  variant="outlined"
                  name="names"
                  value={form.names}
                  fullWidth
                  type="text"
                  className="mb-4"
                  placeholder="Ingresa tu Nombre"
                  focused
                  disabled={loading}
                  onChange={handleForm}
                />
                <TextField
                  id="outlined-basic"
                  label="Phone"
                  variant="outlined"
                  name="phone"
                  fullWidth
                  type="number"
                  value={form.phone}
                  className="mb-4"
                  placeholder="Ingresa tu Telefono"
                  focused
                  disabled={loading}
                  onChange={handleForm}
                />
              </div>
              <div className="col col-12 col-md-6 register__form__datos__right">
                <TextField
                  id="outlined-basic"
                  label="LastNames *"
                  variant="outlined"
                  name="lastNames"
                  value={form.lastNames}
                  fullWidth
                  type="text"
                  className="mb-4"
                  placeholder="Ingresa tus Apellidos"
                  focused
                  disabled={loading}
                  onChange={handleForm}
                />
                <TextField
                  id="outlined-basic"
                  label="Address"
                  variant="outlined"
                  name="address"
                  value={form.address}
                  fullWidth
                  type="text"
                  className="mb-4"
                  placeholder="Ingresa tus Dirección"
                  focused
                  disabled={loading}
                  onChange={handleForm}
                />
              </div>
            </div>
            <div className="row  register__form__datos">
              <div className="col col-12 ">
                <TextField
                  id="outlined-basic"
                  label="Email *"
                  variant="outlined"
                  name="email"
                  value={form.email}
                  fullWidth
                  type="email"
                  className="mb-4"
                  placeholder="Ingresa tu Correo Electrónico"
                  focused
                  disabled={loading}
                  onChange={handleForm}
                />
              </div>
            </div>
            <div className="row register__form__datos">
              <div className="col col-12 col-md-6 register__form__datos__left">
                <TextField
                  id="outlined-basic"
                  label="Password *"
                  variant="outlined"
                  name="password"
                  fullWidth
                  type="password"
                  value={form.password}
                  className="mb-4"
                  placeholder="Ingresa tu Contraseña"
                  focused
                  disabled={loading}
                  onChange={handleForm}
                />
              </div>
              <div className="col col-12 col-md-6 register__form__datos__right">
                <TextField
                  id="outlined-basic"
                  label="Confirm Password *"
                  variant="outlined"
                  name="password2"
                  fullWidth
                  type="password"
                  value={form.password2}
                  className="mb-5"
                  placeholder="Confirma tu Contraseña"
                  focused
                  disabled={loading}
                  onChange={handleForm}
                />
              </div>
            </div>
            <div className="row register__form__datos">
              {/* {error ? (
                <>
                  <div className="register__form__datos__space">
                    <Stack
                      sx={{
                        width: "100%",
                        border: "1px solid red",
                      }}
                      spacing={2}
                    >
                      <Alert severity="error">{error}</Alert>
                    </Stack>
                  </div>
                </>
              ) : (
                <div style={{ height: 10 }}></div>
              )}
              {error2 ? (
                <>
                  <div className="register__form__datos__space">
                    <Stack
                      sx={{
                        width: "100%",
                        border: "1px solid red",
                      }}
                      spacing={2}
                    >
                      <Alert severity="error">{error2}</Alert>
                    </Stack>
                  </div>
                </>
              ) : (
                <div style={{ height: 10 }}></div>
              )} */}
              <div className=" register__form__datos__button">
                {form.password === "" ||
                form.email === "" ||
                form.password2 === "" ||
                form.names === "" ||
                form.lastNames === "" ? (
                  <button type="button" className="button" disabled>
                    Registrarme
                  </button>
                ) : (
                  <button
                    className="button register__form__textButton"
                    disabled={loading}
                  >
                    {loading ? (
                      <RingLoader color={"#fff"} css={override} size={40} />
                    ) : (
                      "Registrarme"
                    )}
                  </button>
                )}
              </div>
            </div>
          </ThemeProvider>
        </form>
      </div>
    </>
  );
};

export default RegisterPage;
