import { Route, Routes } from "react-router-dom";
import MyBox from "../pages/MyBox/MyBox";
import SaveBox from "../pages/SaveBox/SaveBox";
import PerfilPage from "../pages/Perfil/PerfilPage";
import Subscription from "../pages/Subscription/Subscription";

export const DashboardClient = () => {
  return (
    <>
      <Routes>
        <Route path="/perfil" exact={true} element={<PerfilPage />} />
        <Route path="/mibox" exact={true} element={<MyBox />} />
        <Route
          path="/mibox/saveBox/:idBox"
          exact={true}
          element={<SaveBox />}
        />
        <Route path="/suscripciones" exact={true} element={<Subscription />} />
      </Routes>
    </>
  );
};
