import React from "react";
import { BrowserRouter } from "react-router-dom";

import { ValidateRoutes } from "./ValidateRoutes";

export const AppRouter = () => {
  return (
    <BrowserRouter>
      <ValidateRoutes />
    </BrowserRouter>
  );
};
