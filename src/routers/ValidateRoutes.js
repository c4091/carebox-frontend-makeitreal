import { useEffect, useState } from "react";
import { Route, Routes, useLocation } from "react-router-dom";
import Footer from "../components/Footer/Footer";
import NavBar from "../components/Navbar/Navbar";
import ValidatePayment from "../components/ValidatePayment/ValidatePayment";
import AdmAllSubscriptions from "../pages/AdmAllSubscriptions/AdmAllSubscriptions";
import AdmLogin from "../pages/AdmLogin/AdmLogin";
import AdmTodaySubscriptions from "../pages/AdmTodaySubscriptions/AdmTodaySubscriptions";
import Box from "../pages/Box/Box";
import BoxDetails from "../pages/BoxDetails/BoxDetails";
import HomePage from "../pages/Home/HomePage";
import LoginPage from "../pages/Login/LoginPage";
import RegisterPage from "../pages/Register/RegisterPage";
import RestorePasswordPage from "../pages/RestorePassword/RestorePasswordPage";
import { DashboardClient } from "./DashboardClient";
import { PrivateAdminRoutes } from "./PrivateAdminRoutes";
import { PrivateClientRoutes } from "./PrivateClientRoutes";

export const ValidateRoutes = () => {
  const [validar, setValidar] = useState(true);
  let location = useLocation();
  let ubication = location.pathname;

  function usePageViews() {
    useEffect(() => {
      setValidar(ubication.includes("/admin"));
    }, []);
  }
  usePageViews();

  return (
    <>
      {!validar && (
        <NavBar ubication={`${ubication.replaceAll("/", "")}-content`} />
      )}

      <Routes>
        {/* Client */}
        <Route path="/" exact={true} element={<HomePage />} />
        <Route path="/login" exact={true} element={<LoginPage />} />
        <Route
          path="/validatePayment/:idBox/:deliveryDate"
          exact={true}
          element={<ValidatePayment />}
        />
        <Route
          path="/contrasena"
          exact={true}
          element={<RestorePasswordPage />}
        />
        <Route path="/registro" exact={true} element={<RegisterPage />} />
        <Route path="/box" exact={true} element={<Box />} />
        <Route
          path="/:lastRoute/details/:idBox"
          exact={true}
          element={<BoxDetails />}
        />

        {/* admin */}
        <Route path="/admin" exact={true} element={<AdmLogin />} />

        <Route
          path="/admin/*"
          element={
            <PrivateAdminRoutes>
              <>
                <Routes>
                  <Route
                    path="/suscripcionesDiarias"
                    exact={true}
                    element={<AdmTodaySubscriptions />}
                  />
                  <Route
                    path="/suscripciones"
                    exact={true}
                    element={<AdmAllSubscriptions />}
                  />
                </Routes>
              </>
            </PrivateAdminRoutes>
          }
        />

        <Route
          path="/*"
          element={
            <PrivateClientRoutes>
              <DashboardClient />
            </PrivateClientRoutes>
          }
        />
      </Routes>
      {!validar && <Footer />}
    </>
  );
};
